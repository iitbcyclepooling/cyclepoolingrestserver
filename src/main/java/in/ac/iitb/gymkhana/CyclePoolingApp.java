package in.ac.iitb.gymkhana;

import in.ac.iitb.gymkhana.resources.AdminDeviceResource;
import in.ac.iitb.gymkhana.resources.CycleResource;
import in.ac.iitb.gymkhana.resources.UserResource;
import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 *
 */
public class CyclePoolingApp extends Application<CyclePoolingAppConfiguration>{

    private static final Logger logger = LoggerFactory.getLogger(CyclePoolingApp.class);


    public static void main( String[] args ) throws Exception {
        new CyclePoolingApp().run(args);
    }

    @Override
    public void run(CyclePoolingAppConfiguration cyclePoolingAppConfiguration, Environment environment) throws Exception {
        logger.info("Method App#run() called");
        for (int i=0; i < cyclePoolingAppConfiguration.getMessageRepetitions(); i++) {
            System.out.println(cyclePoolingAppConfiguration.getMessage());
        }

        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory
                .build(environment, cyclePoolingAppConfiguration.getDataSourceFactory(), "mysql");
        environment.jersey().register(new CycleResource(jdbi));
        environment.jersey().register(new UserResource(jdbi));
        environment.jersey().register(new AdminDeviceResource(jdbi));
    }
}
