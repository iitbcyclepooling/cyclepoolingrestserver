package in.ac.iitb.gymkhana.mappers;

import in.ac.iitb.gymkhana.Constants;
import in.ac.iitb.gymkhana.model.User;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by akash on 29/4/15 at 5:04 AM.
 */
public class UserMapper implements ResultSetMapper<User> {
    @Override
    public User map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        User user  = new User();
        user.setUserId(resultSet.getInt(Constants.COLUMN_USER_ID));
        user.setUserNfcUid(resultSet.getDouble(Constants.COLUMN_USER_NFC_UID));
        user.setUserName(resultSet.getString(Constants.COLUMN_USER_NAME));
        user.setUserRollNo(resultSet.getString(Constants.COLUMN_USER_ROLL_NO));
        user.setUserHostel(resultSet.getString(Constants.COLUMN_USER_HOSTEL));

        return user;
    }
}
