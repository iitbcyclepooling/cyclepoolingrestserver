package in.ac.iitb.gymkhana.mappers;

import in.ac.iitb.gymkhana.Constants;
import in.ac.iitb.gymkhana.model.Cycle;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by akash on 28/4/15 at 4:58 AM.
 */
public class CycleMapper implements ResultSetMapper<Cycle> {

    @Override
    public Cycle map(int i, ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        Cycle cycle = new Cycle();
        cycle.setCycleId(resultSet.getInt(Constants.COLUMN_CYCLE_ID));
        cycle.setCycleNfcUid(resultSet.getDouble(Constants.COLUMN_CYCLE_NFC_UID));
        cycle.setCycleStatus(resultSet.getInt(Constants.COLUMN_CYCLE_STATUS));
        cycle.setLocatedAt(resultSet.getString(Constants.COLUMN_LOCATED_AT));
        cycle.setExpectedBack(resultSet.getTimestamp(Constants.COLUMN_EXPECTED_BACK));
        cycle.setCreatedAt(resultSet.getTimestamp(Constants.COLUMN_CREATED_AT));
        cycle.setIssuedAt(resultSet.getTimestamp(Constants.COLUMN_ISSUED_AT));
        cycle.setIssueLocation(resultSet.getString(Constants.COLUMN_ISSUE_LOCATION));
        cycle.setIssuedToId(resultSet.getInt(Constants.COLUMN_ISSUED_TO_ID));
        cycle.setIssuedById(resultSet.getInt(Constants.COLUMN_ISSUED_BY_ID));
        cycle.setCycleProblem(resultSet.getString(Constants.COLUMN_CYCLE_PROBLEM));

        return cycle;
    }
}
