package in.ac.iitb.gymkhana.mappers;

import in.ac.iitb.gymkhana.Constants;
import in.ac.iitb.gymkhana.model.AdminDevice;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by akash on 1/5/15 at 3:24 AM.
 */
public class AdminDeviceMapper implements ResultSetMapper<AdminDevice> {
    @Override
    public AdminDevice map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException {
        AdminDevice adminDevice = new AdminDevice();
        adminDevice.setAdminId(resultSet.getInt(Constants.COLUMN_DEVICE_ADMIN_ID));
        adminDevice.setDeviceId(resultSet.getInt(Constants.COLUMN_DEVICE_ID));
        adminDevice.setDeviceUniqueId(resultSet.getString(Constants.COLUMN_DEVICE_UNIQUE_ID));
        adminDevice.setLocatedAt(resultSet.getString(Constants.COLUMN_DEVICE_LOCATED_AT));
        adminDevice.setCreatedAt(resultSet.getTimestamp(Constants.COLUMN_DEVICE_CREATED_AT));

        return adminDevice;
    }
}
