package in.ac.iitb.gymkhana.exception;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 * Created by akash on 28/4/15 at 4:53 AM.
 */
public class ResponseMessageObject {

    @JsonProperty
    @NotEmpty
    private String status;

    @JsonProperty
    @NotEmpty
    private String message;

    public ResponseMessageObject(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
