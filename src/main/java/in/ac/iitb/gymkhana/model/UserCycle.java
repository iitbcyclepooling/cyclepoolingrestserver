package in.ac.iitb.gymkhana.model;

/**
 * Created by akash on 29/4/15 at 3:21 PM.
 */
public class UserCycle {

    private User user;
    private Cycle cycle;

    public UserCycle() {
    }

    public UserCycle(User user, Cycle cycle) {
        this.user = user;
        this.cycle = cycle;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Cycle getCycle() {
        return cycle;
    }

    public void setCycle(Cycle cycle) {
        this.cycle = cycle;
    }
}
