package in.ac.iitb.gymkhana.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.jackson.JsonSnakeCase;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.sql.Timestamp;

/**
 * Created by akash on 28/4/15 at 5:00 AM.
 */

@JsonSnakeCase
@JsonIgnoreProperties
public class Cycle {
    private int cycleId;

    private double cycleNfcUid;

    private int cycleStatus;

    private String locatedAt;

    private Timestamp expectedBack;

    private Timestamp createdAt;

    private Timestamp issuedAt;

    private String issueLocation;

    private int issuedToId;

    private int issuedById;

    private String cycleProblem;

    public Cycle() {
    }

    public Cycle(int cycleId, double cycleNfcUid, int cycleStatus,
                 String locatedAt, Timestamp expectedBack, Timestamp createdAt,
                 Timestamp issuedAt, String issueLocation, int issuedToId,
                 int issuedById, String cycleProblem) {
        this.cycleId = cycleId;
        this.cycleNfcUid = cycleNfcUid;
        this.cycleStatus = cycleStatus;
        this.locatedAt = locatedAt;
        this.expectedBack = expectedBack;
        this.createdAt = createdAt;
        this.issuedAt = issuedAt;
        this.issueLocation = issueLocation;
        this.issuedToId = issuedToId;
        this.issuedById = issuedById;
        this.cycleProblem = cycleProblem;
    }

    public int getCycleId() {
        return cycleId;
    }

    public void setCycleId(int cycleId) {
        this.cycleId = cycleId;
    }

    public double getCycleNfcUid() {
        return cycleNfcUid;
    }

    public void setCycleNfcUid(double cycleNfcUid) {
        this.cycleNfcUid = cycleNfcUid;
    }

    public int getCycleStatus() {
        return cycleStatus;
    }

    public void setCycleStatus(int cycleStatus) {
        this.cycleStatus = cycleStatus;
    }

    public String getLocatedAt() {
        return locatedAt;
    }

    public void setLocatedAt(String locatedAt) {
        this.locatedAt = locatedAt;
    }

    public Timestamp getExpectedBack() {
        return expectedBack;
    }

    public void setExpectedBack(Timestamp expectedBack) {
        this.expectedBack = expectedBack;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getIssuedAt() {
        return issuedAt;
    }

    public void setIssuedAt(Timestamp issuedAt) {
        this.issuedAt = issuedAt;
    }

    public String getIssueLocation() {
        return issueLocation;
    }

    public void setIssueLocation(String issueLocation) {
        this.issueLocation = issueLocation;
    }

    public int getIssuedToId() {
        return issuedToId;
    }

    public void setIssuedToId(int issuedToId) {
        this.issuedToId = issuedToId;
    }

    public int getIssuedById() {
        return issuedById;
    }

    public void setIssuedById(int issuedById) {
        this.issuedById = issuedById;
    }

    public String getCycleProblem() {
        return cycleProblem;
    }

    public void setCycleProblem(String cycleProblem) {
        this.cycleProblem = cycleProblem;
    }

}

