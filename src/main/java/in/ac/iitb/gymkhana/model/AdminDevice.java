package in.ac.iitb.gymkhana.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.dropwizard.jackson.JsonSnakeCase;

import java.sql.Timestamp;

/**
 * Created by akash on 1/5/15 at 3:24 AM.
 */
@JsonSnakeCase
@JsonIgnoreProperties
public class AdminDevice {

    private int deviceId;
    private String deviceUniqueId;
    private int adminId;
    private String locatedAt;
    private Timestamp createdAt;

    public AdminDevice() {
    }

    public AdminDevice(int deviceId, String deviceUniqueId,
                       int adminId, String locatedAt, Timestamp createdAt) {
        this.deviceId = deviceId;
        this.deviceUniqueId = deviceUniqueId;
        this.adminId = adminId;
        this.locatedAt = locatedAt;
        this.createdAt = createdAt;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(int deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceUniqueId() {
        return deviceUniqueId;
    }

    public void setDeviceUniqueId(String deviceUniqueId) {
        this.deviceUniqueId = deviceUniqueId;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public String getLocatedAt() {
        return locatedAt;
    }

    public void setLocatedAt(String locatedAt) {
        this.locatedAt = locatedAt;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }
}
