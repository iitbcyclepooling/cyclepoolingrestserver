package in.ac.iitb.gymkhana.database;

import in.ac.iitb.gymkhana.Constants;
import in.ac.iitb.gymkhana.mappers.CycleMapper;
import in.ac.iitb.gymkhana.model.Cycle;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.sql.Timestamp;

/**
 * Created by akash on 28/4/15 at 5:33 AM.
 */


@RegisterMapper(CycleMapper.class)
public interface CycleDao {

    @SqlUpdate("insert into cycles(cycle_nfc_uid,cycle_status,located_at,created_at) VALUES " +
            "(:cycle_nfc_uid,:cycle_status,:located_at,NOW())")
    void addCycle(@Bind(Constants.COLUMN_CYCLE_NFC_UID) double cycleNfcUid,
                  @Bind(Constants.COLUMN_CYCLE_STATUS) int cycleStatus,
                  @Bind(Constants.COLUMN_LOCATED_AT) String locatedAt);

    @SqlQuery("select * from cycles where cycle_nfc_uid = :cycle_nfc_uid")
    Cycle getCycle(@Bind(Constants.COLUMN_CYCLE_NFC_UID) double cycleNfcUid);

    @SqlQuery("select * from cycles where issued_to_id = :issued_to_id")
    Cycle getIssuedCycle(@Bind(Constants.COLUMN_ISSUED_TO_ID) int issuedToId);

    @SqlUpdate("update cycles set issued_to_id=:issued_to_id,expected_back=:expected_back," +
                "issue_location=:issue_location,cycle_status=:cycle_status " +
                "where cycle_nfc_uid=:cycle_nfc_uid")
    void issueCycle(@Bind(Constants.COLUMN_ISSUED_TO_ID) int studentId,
                    @Bind(Constants.COLUMN_EXPECTED_BACK) Timestamp expectedBack,
                    @Bind(Constants.COLUMN_ISSUE_LOCATION) String issueLocation,
                    @Bind(Constants.COLUMN_CYCLE_NFC_UID) double cycleNfcUid,
                    @Bind(Constants.COLUMN_CYCLE_STATUS) int cycleStatus);

    @SqlUpdate("update cycles set issued_to_id=NULL ,issue_location=NULL ,issued_at=NULL," +
            "cycle_status=:cycle_status , expected_back=NULL,located_at=:located_at where " +
            "cycle_nfc_uid=:cycle_nfc_uid")
    void returnCycle(@Bind(Constants.COLUMN_CYCLE_STATUS) int cycleStatus,
                     @Bind(Constants.COLUMN_LOCATED_AT) String returnedAt,
                     @Bind(Constants.COLUMN_CYCLE_NFC_UID) double cycleNfcUid);

    @SqlUpdate("update cycles set cycle_status=:cycle_status,cycle_problem=:cycle_problem" +
            "located_at=:located_at where cycle_nfc_uid=:cycle_nfc_uid")
    void reportCycle(@Bind(Constants.COLUMN_CYCLE_STATUS) int cycleStatus,
                     @Bind(Constants.COLUMN_CYCLE_PROBLEM) String cycleProblem,
                     @Bind(Constants.COLUMN_LOCATED_AT) String locatedAt,
                     @Bind(Constants.COLUMN_CYCLE_NFC_UID) double cycleNfcUid);

    @SqlUpdate("update cycles set cycle_status=:cycle_status,cycle_problem=NULL," +
            "located_at=:located_at where cycle_nfc_uid=:cycle_nfc_uid")
    void fixCycle(@Bind(Constants.COLUMN_CYCLE_STATUS) int cycleStatus,
                  @Bind(Constants.COLUMN_LOCATED_AT) String returnedAt,
                  @Bind(Constants.COLUMN_CYCLE_NFC_UID) double cycleNfcUid);
}

