package in.ac.iitb.gymkhana.database;

import in.ac.iitb.gymkhana.Constants;
import in.ac.iitb.gymkhana.mappers.AdminDeviceMapper;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

/**
 * Created by akash on 1/5/15 at 3:23 AM.
 */

@RegisterMapper(AdminDeviceMapper.class)
public interface AdminDeviceDao {

    @SqlUpdate("insert ignore into admin_devices(device_unique_id, created_at) " +
            "VALUES (:device_unique_id,NOW())")
    void createDevice(@Bind(Constants.COLUMN_DEVICE_UNIQUE_ID) String deviceUid);

    @SqlUpdate("update admin_devices set located_at =:located_at where device_unique_id" +
            "=:device_unique_id")
    void updateDeviceLocation(@Bind(Constants.COLUMN_DEVICE_LOCATED_AT) String locatedAt,
                              @Bind(Constants.COLUMN_DEVICE_UNIQUE_ID) String deviceUid);
}
