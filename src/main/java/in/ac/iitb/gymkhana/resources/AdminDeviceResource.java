package in.ac.iitb.gymkhana.resources;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import in.ac.iitb.gymkhana.Constants;
import in.ac.iitb.gymkhana.database.AdminDeviceDao;
import in.ac.iitb.gymkhana.exception.ResponseMessageObject;
import in.ac.iitb.gymkhana.model.AdminDevice;
import io.dropwizard.hibernate.UnitOfWork;
import org.skife.jdbi.v2.DBI;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by akash on 1/5/15 at 5:39 AM.
 */
@Path("/device")
@Consumes("application/json")
@Produces("application/json")
public class AdminDeviceResource {
    private final AdminDeviceDao adminDeviceDao;

    public AdminDeviceResource(DBI jdbi){
        adminDeviceDao = jdbi.onDemand(AdminDeviceDao.class);
    }

    @PUT
    @Timed
    @UnitOfWork
    @ExceptionMetered
    @Path("/register")
    public Response registerDevice(AdminDevice adminDevice){
        adminDeviceDao.updateDeviceLocation(adminDevice.getLocatedAt(),adminDevice.getDeviceUniqueId());
        return Response.ok(new ResponseMessageObject(Constants.STATUS_SUCCESS
                ,"Location updated successfully")).build();
    }

    @POST
    @Timed
    @UnitOfWork
    @ExceptionMetered
    @Path("/create")
    public Response createDevice(AdminDevice adminDevice){
        adminDeviceDao.createDevice(adminDevice.getDeviceUniqueId());
        return Response.ok(new ResponseMessageObject(Constants.STATUS_SUCCESS,
                "Device created successfully")).build();
    }
}
