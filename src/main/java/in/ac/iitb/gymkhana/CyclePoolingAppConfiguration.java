package in.ac.iitb.gymkhana;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by akash on 28/4/15 at 8:13 AM.
 */
public class CyclePoolingAppConfiguration extends Configuration {

    @JsonProperty
    private String message;

    @JsonProperty
    private int messageRepetitions;

    @JsonProperty
    private DataSourceFactory database = new DataSourceFactory();
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

    public String getMessage() {
        return message;
    }

    public int getMessageRepetitions() {
        return messageRepetitions;
    }

}
