package in.ac.iitb.gymkhana;

/**
 * Created by akash on 28/4/15 at 5:19 AM.
 */
public class Constants {

    public static final String STATUS_SUCCESS= "success";
    public static final String STATUS_ERROR= "error";


    /**
     * Cycle statuses
     */
    public static final int CYCLE_STATUS_AVAILABLE = 0;
    public static final int CYCLE_STATUS_ISSUED = 1;
    public static final int CYCLE_STATUS_REPAIR = 2;




    /**
     * Cycle table columns
     */
    public static final String COLUMN_CYCLE_ID = "cycle_id";
    public static final String COLUMN_CYCLE_NFC_UID = "cycle_nfc_uid";
    public static final String COLUMN_CYCLE_STATUS  = "cycle_status";
    public static final String COLUMN_LOCATED_AT = "located_at";
    public static final String COLUMN_EXPECTED_BACK = "expected_back";
    public static final String COLUMN_CREATED_AT = "created_at";
    public static final String COLUMN_ISSUED_AT = "issued_at";
    public static final String COLUMN_ISSUE_LOCATION = "issue_location";
    public static final String COLUMN_ISSUED_TO_ID = "issued_to_id";
    public static final String COLUMN_ISSUED_BY_ID = "issued_by_id";
    public static final String COLUMN_CYCLE_PROBLEM = "cycle_problem";

    /**
     * User table columns
     */
    public static final String COLUMN_USER_ID ="user_id";
    public static final String COLUMN_USER_NFC_UID = "user_nfc_uid";
    public static final String COLUMN_USER_NAME = "user_name";
    public static final String COLUMN_USER_ROLL_NO = "user_roll_no";
    public static final String COLUMN_USER_HOSTEL = "user_hostel";

    /**
     * Admin device table columns
     */

    public static final String COLUMN_DEVICE_ID = "device_id";
    public static final String COLUMN_DEVICE_UNIQUE_ID = "device_unique_id";
    public static final String COLUMN_DEVICE_LOCATED_AT = "located_at";
    public static final String COLUMN_DEVICE_ADMIN_ID = "admin_id";
    public static final String COLUMN_DEVICE_CREATED_AT = "created_at";



}
